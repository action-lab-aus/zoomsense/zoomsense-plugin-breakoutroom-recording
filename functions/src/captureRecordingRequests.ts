import { typedFirebaseFunctionRef, ZoomSenseServerFirebaseDB } from "@zoomsense/zoomsense-firebase";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

import { BreakoutRecordingPluginEventType } from "../src-shared";

const db = new ZoomSenseServerFirebaseDB(admin.database());

// [start|stop][whitespace][recording]
const startRecordingPattern = /^start\s*recording$/i;
const stopRecordingPattern = /^stop\s*recording$/i;

export const captureRecordingRequests = typedFirebaseFunctionRef(functions.database.ref)(
  "data/chats/{meetingId}/{sensorId}/{chatId}"
)
  .onCreate(async (snapshot, context) => {
    const { meetingId, sensorId } = context.params;

    try {
      const recordingConfig = (
        await db
          .ref(`config/${meetingId}/current/currentState/plugins/recording`)
          .once("value")
      ).val();
      if (!recordingConfig || !recordingConfig.enabled) return;
      const {
        msg: messageContent,
        msgReceiverName,
        msgSender,
        msgSenderName,
        timestamp,
      } = snapshot.val()!;

      if (
        !messageContent ||
        !msgReceiverName ||
        !msgSender ||
        !msgSenderName ||
        !timestamp
      ) {
        return;
      }

      if (messageContent.trim().match(startRecordingPattern)) {
        await db
          .ref(`data/plugins/boRecording/${meetingId}/${sensorId}/events`)
          .push({
            type: BreakoutRecordingPluginEventType.start,
            zoomUserId: msgSender,
            userName: msgSenderName,
            timestamp: timestamp,
          });
        await db
          .ref(`data/chats/${meetingId}/${sensorId}/message`)
          .push({
            msg: "The current time has been saved as the start of the recording.  Type \"stop recording\" in the chat to stop.",
            receiver: msgSender,
          });
      }

      if (messageContent.trim().match(stopRecordingPattern)) {
        await db
          .ref(`data/plugins/boRecording/${meetingId}/${sensorId}/events`)
          .push({
            type: BreakoutRecordingPluginEventType.stop,
            zoomUserId: msgSender,
            userName: msgSenderName,
            timestamp: timestamp,
          });

        await db
          .ref(`data/chats/${meetingId}/${sensorId}/message`)
          .push({
            msg: "Is this your final version? Type \"start recording\" in the chat if you need to record again.",
            receiver: msgSender,
          });
      }
    } catch (e) {
      functions.logger.error("captureRecordingRequests: ", e);
    }
  });
