# Shared local package

This directory defines a local package, containing code that is shared by the back-end functions and front-end VUE code.
Because Firebase zips up the `functions` folder when it deploys to the cloud and builds its dependencies from scratch,
it needs to exist in the `functions` folder, so it's available when building in the cloud.
