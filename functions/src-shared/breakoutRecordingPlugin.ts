import "@zoomsense/zoomsense-firebase";

declare module "@zoomsense/zoomsense-firebase" {
  export interface ZoomSenseDataPlugins {
    boRecording: BreakoutRecordingPlugin;
  }
}

export enum BreakoutRecordingPluginEventType {
  "start" = "start",
  "stop" = "stop"
}

export interface BreakoutRecordingPluginEvent {
  type: BreakoutRecordingPluginEventType;
  zoomUserId: number;
  userName: string;
  timestamp: number;
}

export interface BreakoutRecordingPluginMeeting {
  [sensorName: string]: {
    events?: {
      [eventId: string]: BreakoutRecordingPluginEvent;
    }
  }
}

export interface BreakoutRecordingPlugin {
  [meetingId: string]: BreakoutRecordingPluginMeeting;
}

