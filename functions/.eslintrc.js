module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  plugins: ["@typescript-eslint"],
  extends: ["plugin:@typescript-eslint/base"],
  rules: {
    quotes: ["error", "double"],
    "quote-props": "off",
    "object-curly-spacing": ["error", "always"],
  },
};
